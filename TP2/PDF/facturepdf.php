<?php

    require('../FPDF/fpdf.php');
    class PDF extends FPDF {

        var $widths;
        var $aligns;

        function SetWidths($w)
        {
            $this->widths=$w;
        }

        function SetAligns($a)
        {
            $this->aligns=$a;
        }

        function Row($data)
        {
            $nb=0;
            for($i=0;$i<count($data);$i++)
                $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
            $h=5*$nb;
            $this->CheckPageBreak($h);
            for($i=0;$i<count($data);$i++)
            {
                $w=$this->widths[$i];
                $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
                $x=$this->GetX();
                $y=$this->GetY();
                $this->Rect($x,$y,$w,$h);
                $this->MultiCell($w,5,$data[$i],0,$a);
                $this->SetXY($x+$w,$y);
            }
            $this->Ln($h);
        }

        function CheckPageBreak($h)
        {
            if($this->GetY()+$h>$this->PageBreakTrigger)
                $this->AddPage($this->CurOrientation);
        }

        function NbLines($w,$txt)
        {
            $cw=&$this->CurrentFont['cw'];
            if($w==0)
                $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r",'',$txt);
            $nb=strlen($s);
            if($nb>0 and $s[$nb-1]=="\n")
                $nb--;
            $sep=-1;
            $i=0;
            $j=0;
            $l=0;
            $nl=1;
            while($i<$nb)
            {
                $c=$s[$i];
                if($c=="\n")
                {
                    $i++;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                    continue;
                }
                if($c==' ')
                    $sep=$i;
                $l+=$cw[$c];
                if($l>$wmax)
                {
                    if($sep==-1)
                    {
                        if($i==$j)
                            $i++;
                    }
                    else
                        $i=$sep+1;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                }
                else
                    $i++;
            }
            return $nl;
        }

        function Header() {
            $this->SetFont('Arial','B',15);
            $this->Cell(80);
            $this->Cell(30,10,'FACTURE',0,1,'C');
            $this->Cell(80);
            $this->Cell(30,10,'SALON DE LA DECORATION DE LILLE',0,1,'C');
        }

        function Table($header, $data, $decalage, $width, $titrealign, $datalign)
        {
            $this->SetWidths($width);
            $this->SetAligns($titrealign);
            $this->SetFont('Arial','B',10);
            $this->Cell($decalage);
            $this->Row($header);
            $this->SetFont('Arial','',8);
            foreach($data as $row)
            {
                $this->Cell($decalage);
                $this->SetAligns($datalign);
                $this->Row($row);
            }
        }
    }

    if(isset($_GET['facture'])){
        if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
            $sql = $db->prepare("SELECT * FROM Client c, Commande co WHERE co.numclient=c.numclient AND co.numfacture = ?");
            $sql->bind_param("s" , $_GET['facture']);   
            if($sql->execute()){
                $result = $sql->get_result();
                $data = $result->fetch_assoc();                    
            }                     
            $sql->close();
            mysqli_close($db);
        }
        if(!isset($data['nom'])){
            header("Location: ../Reservation/index.php");
            exit;
        }               
        if($data['sexe'] == 1){
            $nom = "Mr " . $data['prenom'] . " " . $data['nom'];
        }
        else{
            $nom = "Mme " . $data['prenom'] . " " . $data['nom'];
        }
    }
    else{
        header("Location: ../Reservation/index.php");
        exit;
    }


    $header = array(utf8_decode("Client Facturé"), "", utf8_decode("Références"));
    $val = array(array(utf8_decode($nom . "\n" . $data['adresse'] . "\n" . $data['codepostal'] . " " . $data['ville']), "", utf8_decode("N°Commande : " . $data['numcommande'] . "\nN°Facture : " . $data['numfacture'] . "\nDate de la facture : " . date('d-m-Y') . "\nN°Client : " . $data['numclient'] )));
    $pdf = new PDF();
    $pdf->AddPage();
    $pdf->Ln();
    $pdf->Table($header, $val, 20, array(70, 10, 70), array('C', 'C', 'C'), null);



    if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
        $sql = $db->prepare("SELECT b.prix, COUNT(b.numbillet) FROM Client c, Commande co, Billet b WHERE co.numclient=c.numclient AND b.numcommande = co.numcommande AND c.numclient = ? GROUP BY b.prix");
        $sql->bind_param("s" , $data['numclient']);   
        if($sql->execute()){
            $total = 0;
            $result = $sql->get_result();
            while($billet = $result->fetch_assoc()){
                if($billet['prix'] == 8){
                    $val2[] = array(utf8_decode("Entrée Plein Tarif"), $billet['COUNT(b.numbillet)'], utf8_decode("8.00 euros"), "-", utf8_decode(number_format($billet['prix'] * $billet['COUNT(b.numbillet)'],2) . " euros"));
                }
                else{
                    $val2[] = array(utf8_decode("Entrée Plein Tarif"), $billet['COUNT(b.numbillet)'], utf8_decode("8.00 euros"), "50 %", utf8_decode(number_format($billet['prix'] * $billet['COUNT(b.numbillet)'],2) . " euros"));
                }
                $total += $billet['prix'] * $billet['COUNT(b.numbillet)'];
            }                   
        }                     
        $sql->close();
        mysqli_close($db);
    }



    $header2 = array(utf8_decode("Désignation"), utf8_decode("Qté"), utf8_decode("Prix Unitaire TTC"), utf8_decode("Remise"), utf8_decode("Montant TTC"));
    $pdf->Ln();
    $pdf->Ln();
    $pdf->Table($header2, $val2, 20, array(50, 20, 30, 20, 30), array('C', 'C', 'C', 'C', 'C'), array('C', 'C', 'C', 'C', 'C'));

    $header3 = array("Total HT", "Total TVA 5.5%", "Total TTC");
    $val3 = array(array(number_format($total/(1.055), 2) . " euros", number_format($total - ($total/(1.055)), 2) . " euros", $total . " euros"));
    $pdf->Ln();
    $pdf->Ln();
    $pdf->Table($header3, $val3, 80, array(30, 30, 30), array('C', 'C', 'C'), array('C', 'C', 'C'));

    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','',10);
    $pdf->MultiCell(0, 10, utf8_decode("Facture libellée en euros.\nRèglement effectué par Carte Bancaire."));

    $pdf->Output("D", "facture_" . $data['numfacture'] .  ".pdf");
?>
<?php

require('../FPDF/fpdf.php');
class PDF extends FPDF {

    function Header() {
        $this->SetFont('Arial','',20);
        $this->Cell(30,10,utf8_decode('Salon de la décoration de Lille'),0,1,'L');
    }
}

if(isset($_GET['billet'])){
    if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
        $sql = $db->prepare("SELECT * FROM Billet WHERE numbillet = ?");
        $sql->bind_param("s" , $_GET['billet']);   
        if($sql->execute()){
            $result = $sql->get_result();
            $data = $result->fetch_assoc();                    
        }                     
        $sql->close();
        mysqli_close($db);
        
    }
    if(!isset($data['numbillet'])){
        header("Location: ../Reservation/index.php");
        exit;
    }         
}
else{
    header("Location: ../Reservation/index.php");
    exit;
}



$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$pdf->Ln();
$pdf->Cell(45);
$pdf->Cell(30, 40, utf8_decode("Billet électronique à présenter à l'entrée du salon"));
$pdf->Ln();
$pdf->SetFont('Arial','',12);
$pdf->Cell(65);
$pdf->Cell(25, 0, utf8_decode("Numéro : " . $data['numbillet']));
$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->Cell(65);
if($data['prix'] == 4){
    $pdf->Cell(25, 10, utf8_decode("Entrée demi-tarif : 4 euros"));
}
else{
    $pdf->Cell(25, 10, utf8_decode("Entrée plein tarif : 8 euros"));
}
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','',11);
$pdf->Cell(30);
$pdf->MultiCell(200, 5, utf8_decode("Ce billet donne droit à un accès unique au salon. Toute sortie est définitive.\nPour plus d'informations veuillez consulter les conditions générales de vente."));


$pdf->Output("D", "e-billet" . $data['numbillet'] . ".pdf");
?>
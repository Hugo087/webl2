<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Gestion des commandes </title>
        <link rel="stylesheet" href="stylesIndex.css">
        <script src="script.js"></script>
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Back-Office Client <br>Gestion des commandes</div>
        </header>
        <main>
            <table>
                <tr>
                    <th>
                        Date commande
                    </th>
                    <th>
                        Numéro commande
                    </th>
                    <th>
                        Client
                    </th>
                    <th>
                        Numéro client
                    </th>
                    <th>
                        Prix TTC
                    </th>
                    <th>
                        Etat
                    </th>
                    <th>

                    </th>
                </tr>
                <?php
                    ini_set('display_errors', 1);
                    ini_set('display_startup_errors', 1);
                    error_reporting(E_ALL);
                    if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
                        $total = 0;
                        $nombre = 0;
                        $sql="SELECT co.datecommande, co.numcommande,co.numfacture , c.numclient, c.nom, c.prenom, SUM(b.prix) FROM Client c, Commande co, Billet b WHERE b.numcommande=co.numcommande AND co.numclient=c.numclient GROUP BY co.numcommande";
                        $req = mysqli_query($db, $sql) or die(mysqli_error($db));
                        while($data = mysqli_fetch_assoc($req)){
                            $total += $data['SUM(b.prix)'];
                            $nombre += 1;
                            echo "<tr><td>" . $data['datecommande'] . "</td><td>" . $data['numcommande'] . "</td><td>". $data['prenom'] . " " . $data['nom'] . "</td><td>" . $data['numclient'] . "</td><td>" . $data['SUM(b.prix)'] . " € </td><td> Paiment accepté </td><td>" . "<a href='detail.php?numClient=" . $data['numclient'] .  "'>Détails</a> - <a href='../PDF/facturepdf.php?facture=" . $data['numfacture'] .  "'>Facture</a> - <a href=''>Envoyer les billets</a>" . "</td></tr>";
                        }
                        mysqli_close($db);
                        if($nombre == 0){
                            $nombre = 1 ;
                        }
                        echo "<p style='float:right; font-size: 0.8em; margin-right:8em;'><b>Total des commandes : " . $total . " € TTC | Panier Moyen : " . number_format($total/$nombre, 2) . " € TTC</b></p>";
                    }                               
                ?>
            </table>
        </main>
    </body>
</html>
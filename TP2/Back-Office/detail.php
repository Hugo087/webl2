<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if(isset($_GET['numClient'])){
    if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
        $sql = $db->prepare("SELECT * FROM Client c, Commande co WHERE co.numclient=c.numclient AND c.numclient = ?");
        $sql->bind_param("s" , $_GET['numClient']);   
        if($sql->execute()){
            $result = $sql->get_result();
            $data = $result->fetch_assoc();                    
        }                     
        $sql->close();
        mysqli_close($db);
    }
    if(!isset($data['nom'])){
        header("Location: index.php");
        exit;
    }                               
    if($data['sexe'] == 1){
        $nom = "Mr " . $data['prenom'] . " " . $data['nom'];
    }
    else{
        $nom = "Mme " . $data['prenom'] . " " . $data['nom'];
    }
}
else{
    header("Location: index.php");
    exit;
}
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Gestion des commandes </title>
        <link rel="stylesheet" href="stylesDetail.css">
        <script src="script.js"></script>
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Back-Office Client <br>Gestion des commandes</div>
        </header>
        <main>
            <table style="border:none">
                <tr>
                    <td style="vertical-align: top">
                        Commande n° <?=$data['numcommande']?>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <th>
                                    Client facturé
                                </th>
                                <th>
                                    Références
                                </th>
                            </tr>
                            <tr>
                                <td rowspan="1">
                                    <?= $nom ?>
                                </td>
                                <td>
                                    N° Commande : <?=$data['numcommande']?>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="1">
                                    <?= $data['adresse']?>
                                </td>
                                <td>
                                    N° Facture : <?= $data['numfacture'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2">
                                    <?= $data['codepostal'] . " " . $data['ville'] ?>
                                </td>
                                <td>
                                    Date de la Facture : <?=$data['datecommande']?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    N° Client : <?=$data['numclient']?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <th>
                                    Désignation
                                </th>
                                <th>
                                    Quantité
                                </th>
                                <th>
                                    Prix Unitaire TTC
                                </th>
                                <th>
                                    Remise
                                </th>
                                <th>
                                    Montant TTC
                                </th>
                            </tr>
                            <?php
                                if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
                                    $sql = $db->prepare("SELECT b.prix, COUNT(b.numbillet) FROM Client c, Commande co, Billet b WHERE co.numclient=c.numclient AND b.numcommande = co.numcommande AND c.numclient = ? GROUP BY b.prix");
                                    $sql->bind_param("s" , $_GET['numClient']);   
                                    if($sql->execute()){
                                        $total = 0;
                                        $result = $sql->get_result();
                                        while($billet = $result->fetch_assoc()){
                                            if($billet['prix'] == 8){
                                                echo "<tr><td> Entrée Plein Tarif </td><td> " . $billet['COUNT(b.numbillet)'] . "</td><td> 8.00 € </td><td> - </td><td>" . number_format($billet['prix'] * $billet['COUNT(b.numbillet)'],2) . " €</td></tr>";
                                            }
                                            else{
                                                echo "<tr><td> Entrée Tarif Réduit</td><td> " . $billet['COUNT(b.numbillet)'] . "</td><td> 8.00 € </td><td> 50% </td><td>" . number_format($billet['prix'] * $billet['COUNT(b.numbillet)'], 2) . " €</td></tr>";
                                            }
                                            $total += $billet['prix'] * $billet['COUNT(b.numbillet)'];
                                        }                   
                                    }                     
                                    $sql->close();
                                    mysqli_close($db);
                                }
                            ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <table style="width:50%">
                            <tr>
                                <th>
                                    Total HT
                                </th>
                                <th>
                                    Total TVA 5.5%
                                </th>
                                <th>
                                    Total TTC
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <?=number_format($total/(1.055), 2)?> €
                                </td>
                                <td>
                                    <?=number_format($total - ($total/(1.055)), 2)?> €
                                </td>
                                <td>
                                    <?=$total?> €
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <input type="button" value="Retour" onclick="document.location.href='index.php'" style="float: right; margin-right: 10em;">
        </main>
    </body>
</html>
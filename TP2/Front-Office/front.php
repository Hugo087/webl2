<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Salon de la décoration </title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Front-Office <br><br> Gestion des entrées</div>
        </header>
        <main style="padding-top: 6%; padding-bottom: 6%;">
            <form action="connecter.php" method="post">
                <div class="input">
                    <label>Votre identifiant :</label>
                    <input type="text" name="user">
                </div>
                <div class="input">
                    <label> Code secret :  </label>
                    <input type="password" name="mdp">
                </div>
                <input type="submit" value="Valider" class="butval" style="margin-left: 60%;">
            </form>
        </main>
    </body>
</html>
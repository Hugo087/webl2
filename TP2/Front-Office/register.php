<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Salon de la décoration </title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Front-Office <br><br> Creation utilisateur</div>
        </header>
        <main>
            <?php
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
                if(isset($_POST["user"]) && isset($_POST["mdp"])){
                    $pseudo = $_POST["user"];
                    $mdp = $_POST["mdp"];
                    $hashmdp = password_hash($mdp, PASSWORD_DEFAULT);
                    if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
                        $sql = $db->prepare("INSERT INTO Utilisateur (user, mdp) VALUES (? , ?)");
                        $sql->bind_param("ss" , $pseudo, $hashmdp);
                        if($sql->execute()){
                            echo "<div style='text-align: center'>Utilisateur créé avec succès !</div>";
                        }
                        else{
                            echo "<div style='text-align: center'>Ce nom d'utilisateur existe déjà.</div>";
                        }
                    }
                    else{
                        echo "<div style='text-align: center'>Impossible de contacter le serveur.</div>";
                    }                                       
                    $sql->close();
                    mysqli_close($db);
                }
            ?>
            <form action="register.php" method="post"> 
                <div class="input">
                    <label>Votre identifiant :</label>
                    <input type="text" name="user" required>
                </div>
                <div class="input">
                    <label> Code secret :  </label>
                    <input type="password" name="mdp" required>
                </div>
                <input type="submit" value="Valider" class="butval">
            </form>
        </main>
    </body>
</html>
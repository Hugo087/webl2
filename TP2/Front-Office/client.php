<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body class="gestion">
        <form method="post">
            Nom de la personne apparaissant sur la facture
            <br>
            <br>
            <input type="text" name="nomclient" style="width:20em; "value="<?=$nomclient?>"><input type="submit" value="valider" class="butval">
        </form>
        <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            $nomclient = null;
            if(isset($_POST['nomclient'])){
                $nomclient = $_POST['nomclient'];
                if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
                    $sql = $db->prepare("SELECT c.nom, c.prenom, COUNT(b.numbillet), COUNT(b.datescan) FROM Client c, Commande co, Billet b WHERE co.numclient=c.numclient AND b.numcommande = co.numcommande AND UPPER(c.nom) = UPPER(?) GROUP BY c.numclient" );
                    $sql->bind_param("s" , $nomclient);   
                    if($sql->execute()){
                        $result = $sql->get_result();
                        echo "<b>Résultat(s) de la recherche</b><br><br>";
                        if($result->num_rows == 0){
                            echo "<table><tr><th style='background-color: red; color:white'>Erreur : Nom inconnu</th></tr></table>";
                        }
                        else{
                            echo "<table><tr><th>Nom</th><th>Prénom</th><th>Nombre de billets</th><th>Etat</th></tr>";
                            while($data = $result->fetch_assoc()){
                                if($data['COUNT(b.datescan)'] == 0){
                                    echo "<tr><td>" . $data['nom'] . "</td><td>" . $data['prenom'] . "</td><td>" . $data['COUNT(b.numbillet)'] . "</td><td> non scannés</td></tr>";
                                }
                                else{
                                    echo "<tr><td>" . $data['nom'] . "</td><td>" . $data['prenom'] . "</td><td>" . $data['COUNT(b.numbillet)'] . "</td><td>" . $data['COUNT(b.datescan)'] . " billets scannés</td></tr>";
                                }           
                            }
                            
                            echo "</table>";  
                        }                  
                    }                     
                    $sql->close();
                    mysqli_close($db);
                }
            }
        ?>
    <body>
</html>
<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    session_start();
    if(isset($_POST['user']) && isset($_POST['mdp'])){
        $user = $_POST['user'];
        $mdp = $_POST['mdp'];
        $v = false;
        if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
            $sql = $db->prepare("SELECT user, mdp FROM Utilisateur WHERE user = ?");
            $sql->bind_param("s" , $user);
            if($sql->execute()){
                $sql->bind_result($pseudo, $mdphash);
                $sql->fetch();                        
                if($pseudo === $user && password_verify($mdp, $mdphash)){
                    $_SESSION['pseudo'] = $pseudo;
                    $v = true;
                }
            }
        }                               
        $sql->close();
        mysqli_close($db);
        if($v == false){
            session_destroy();
            header("Location: front.php");
            exit;
        }
    }
    if(!isset($_SESSION['pseudo'])){
        session_destroy();
        header("Location: front.php");
        exit;
    }
?>
<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Salon de la décoration </title>
        <link rel="stylesheet" href="styles.css">
        <script src="script.js"></script>
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Front-Office <br><br> Gestion des entrées</div>
        </header>
        <main>
        <ul id="onglet">
            <li id="actif" class="gestionBillet" onclick="changerActif(this)">Gestion des billets</li>
            <li id="" class="findClient" onclick="changerActif(this)">Trouver un client</li>
            <li id="" class="deconnexion"><a href="deconnexion.php"> Déconnexion</a></li>
        </ul>
        <iframe id="html" src="gestion.php">
        </iframe>
        </main>
    </body>
</html>
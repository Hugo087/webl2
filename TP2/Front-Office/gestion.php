<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $billet = null;
    if(isset($_POST['billet'])){
        $billet = $_POST['billet'];
        if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
            $sql = $db->prepare("SELECT * FROM Client c, Commande co, Billet b WHERE co.numclient=c.numclient AND b.numcommande = co.numcommande AND b.numbillet = ?");
            $sql->bind_param("s" , $billet);   
            if($sql->execute()){
                $result = $sql->get_result();
                $data = $result->fetch_assoc();                   
            }                     
            $sql->close();
            mysqli_close($db);
        }
        if($data['numbillet']){
            if($data['sexe'] = "H"){
                $nom = "Mr " . $data['nom'] . " " . $data['prenom'];
            }
            else{
                $nom = "Mme " . $data['nom'] . " " . $data['prenom'];
            }
        }
    }
?>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body class="gestion">
        <form method="post">
            Scannez ou entrez ici le numéro du billet (13 caractères)
            <br>
            <br>
            <input type="text" name="billet" style="width:20em;" maxlength="13" value="<?=$billet ?>"><input type="submit" value="valider" class="butval">
        </form>
        <br>
        <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            if(isset($billet)){
                if(isset($data['numbillet'])){
                    if(isset($data['datescan'])){
                        echo "<p class='invalide'>Billet déjà scanné le " . $data['datescan'] . "</p>";
                    }
                    else{
                        echo "<p class='valide'>Billet Valide</p>";
                        if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
                            $sql = $db->prepare("UPDATE Billet SET datescan = now() WHERE numbillet = ?");
                            $sql->bind_param("s" , $billet);
                            $sql->execute();
                            $sql->close();
                            mysqli_close($db);
                        }
                    }
                }
                else{
                    echo "<p class='invalide'>Erreur : Billet Inconnu </p>";
                }
            }
            if(isset($data['numbillet'])){
                echo "<div class='gauche'>";
                echo "<b>Informations commande</b><br><br>";
                if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
                    $sql = $db->prepare("SELECT COUNT(numbillet) FROM Billet WHERE numcommande = ?");
                    $sql->bind_param("s" , $data['numcommande']);   
                    if($sql->execute()){
                        $result = $sql->get_result();
                        $nbbillets = $result->fetch_assoc();
                        echo $nbbillets['COUNT(numbillet)'] . " billets achetés";                 
                    }                     
                    $sql->close();
                    mysqli_close($db);
                }
                echo "</div>";
                echo "<div class='droite'>";
                echo $nom;
                echo "<br><br>";
                echo $data['adresse'];
                echo "<br><br>";
                echo $data['codepostal'] . " " . $data['ville'];
                echo "<br><br>";
                echo $data['email'];
                echo "</div>";
            }
            ?>
        </div>
    <body>
</html>
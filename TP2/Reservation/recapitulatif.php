<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if(isset($_POST['billets']) && isset($_POST['type'])&& isset($_POST['sexe'])&& isset($_POST['nom'])&& isset($_POST['prenom'])&& isset($_POST['adresse'])&& isset($_POST['codepostal'])&& isset($_POST['email'])&& isset($_POST['ville'])){
    $billets = $_POST['billets'];
    if($billets > 1){
        $prixbar = $billets * 8  . " € TTC";
    }
    else{
        $prixbar = null;
    }
    $prix = 8 + ($billets - 1) * 4 . " € TTC";
    $pro = "Vous êtes un " . $_POST['type'];
    if($_POST['sexe'] == "H"){
        $nom = "Mr " . $_POST['nom'] . " " . $_POST['prenom'];
    }
    else{
        $nom = "Mme " . $_POST['nom'] . " " . $_POST['prenom'];
    }
    $adresse = $_POST['adresse'] . " " . $_POST['adresse2'];
    $ville = $_POST['codepostal'] . " " . $_POST['ville'];
    $email = $_POST['email'];
    setcookie("nom", $_POST['nom'], time() + 3600 * 24);
    setcookie("prenom", $_POST['prenom'], time() + 3600 * 24);
    setcookie("adresse", $adresse, time() + 3600 * 24);
    setcookie("codepostal", $_POST['codepostal'], time() + 3600 * 24);
    setcookie("ville", $_POST['ville'], time() + 3600 * 24);
    setcookie("type", $_POST['type'], time() + 3600 * 24);
    setcookie("email", $email, time() + 3600 * 24);
    setcookie("sexe", $_POST['sexe'], time() + 3600 * 24);
    setcookie("billets", $billets, time() + 3600 * 24);
}
else{
    header("Location: index.php");
    exit;
}
?>
<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Salon de la décoration </title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Récapitulatif des informations saisies</div>
        </header>
        <main>
            <table>
                <tr>
                    <th>
                        Votre Réservation
                    </th>
                    <th>
                        Nombre de billets
                    </th>
                    <th>
                        Offre Web
                    </th>
                    <th>
                        Total TTC
                    </th>
                </tr>
                <tr>
                    <td>
                        Salon de la décoration Lille Grand Palais<br>
                        6, 7, 8 Mars 2019
                    </td>
                    <td>
                        <?=$billets?>
                    </td>
                    <td>
                        1 entrée achetée la deuxième à moitié prix
                    </td>
                    <td>
                    <p id="prixbar" style="text-align: center; font-size: small; text-decoration: line-through"><?=$prixbar?> </p>
                    <p id="prix" style="color:fuchsia; text-align: center; font-weight: bold"><?=$prix?></p>
                    </td>
                </tr>
            </table>
            <table>

                <tr>
                    <th>
                        Récapitulatif des informations saisies précedemment
                    </th>
                </tr>
                <tr>
                    <td>
                        <?=$pro?>
                        <br>
                        <br>
                        <?=$nom?>
                        <br>
                        <br>
                        <?=$adresse?>
                        <br>
                        <br>
                        <?=$ville?>
                        <br>
                        <br>
                        <?=$email?>
                    </td>
                </tr>
            </table>
            <form method="POST" action="email.php">
                <input type="checkbox" style="margin-left: 8em;" required> J'ai lu et j'accepte <a href="">les conditions générales de vente.</a>
                <input type="submit" value="Valider" class="submit" name="boutonsubmit" style="margin-right: 8em;">
                <input type="submit" value="Modifier" class="submit" onclick="javascript:history.back()" style="margin-right: 1em;">
            </form>
            
        </main>
    </body>
</html>
<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Salon de la décoration </title>
        <link rel="stylesheet" href="styles.css">
        <script src="script.js"></script>
    </head>
    <body onload="chargerprix();">
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Offre uniquement sur Internet : 1 entrée achetée la deuxième à moitié prix !</div>
        </header>
        <main>
            <form action="recapitulatif.php" method="POST">
                <table>
                    <tr>
                        <th>Votre réservation</th>
                        <th>Nombre de billets</th>
                        <th>Offre Web</th>
                        <th>Total TTC</th>
                    </tr>
                    <tr>
                        <td style="text-align: center">Salon de la décoration Lille Grand Palais<br>6,7,8 Mars 2019</td>
                        <td style="text-align: center"><input onchange="chargerprix();" name="billets" id="billets" type="number" min="1" required></td>
                        <td style="color:fuchsia; text-align: center; font-weight: bold">La 2ème à motié prix pour une entréee achetée </td>
                        <td><p id="prixbar" style="text-align: center; font-size: small; text-decoration: line-through"></p><p id="prix" style="color:fuchsia; text-align: center; font-weight: bold"></p></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <th>
                            Vous êtes ?
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="radio" name="type" value="particulier" required> Un Particulier 
                            <input type="radio" name="type" value="professionel" required> Un Professionnel
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <th>
                            Adresse de facturation
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <label>
                                    Civilité :
                                </label>
                                <select name="sexe" required>
                                    <option value="" hidden>Choisissez...</option>
                                    <option value="H">Homme</option>
                                    <option value="F">Femme</option>
                                </select>
                            </div>
                            <div class="droite">
                                    <label>
                                        Prénom : <span style="color:fuchsia">*</span>
                                    </label>
                                    <input name="prenom" type="text" required>
                                </div>
                            <div>
                                <label>
                                    Nom : <span style="color:fuchsia">*</span>
                                </label>
                                <input name="nom" type="text" required>
                            </div>
                            <div class="droite">
                                <label>
                                    Code postal : <span style="color:fuchsia">*</span>
                                </label>
                                <input name="codepostal" type="text" required>
                            </div>
                            <div>
                                <label>
                                    Adresse : <span style="color:fuchsia">*</span>
                                </label> 
                                <input name="adresse" type="text" required>
                            </div>
                            <div class="droite">
                                    <label>
                                        Ville : <span style="color:fuchsia">*</span>
                                    </label>
                                    <input name="ville" type="text" required>
                                </div>
                            <div>
                                <label>
                                </label>
                                <input name="adresse2" type="text">
                            </div>
                            <div class="droite">
                                    <label>
                                        Confirmez votre email : <span style="color:fuchsia">*</span>
                                    </label>
                                    <input name="checkemail" id="checkemail" type="text" required oninput="verifMail()">
                                </div>
                            <div>
                                <label>
                                    Email : <span style="color:fuchsia">*</span>
                                </label>
                                <input name="email" id="email" type="text" required oninput="verifMail()">
                            </div>
                            <br>
                            <p style="color: red"> Un email valide est indispensable car la réception des billets se fait par email.</p>
                            <input type="checkbox"> Cochez la case pour être informé des bons plans du salon
                            <br>
                            <input type="submit" value="Confirmer" class="submit"> 
                            <p style="color:fuchsia"> *Renseignements obligatoires.</p>
                        </td>
                    </tr>
                </table>
            </form>
        </main>
    </body>
</html>
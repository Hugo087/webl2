function chargerprix(){
    var billets = document.getElementById("billets").value;
    var prix = 0;
    if(billets == 1){
        prix = 8;
        document.getElementById("prixbar").innerHTML = "";
    }
    else if(billets > 0){
        prix = 8;
        for(var i = 1; i < billets; i++){
            prix += 4;
        }
        document.getElementById("prixbar").innerHTML = 8 * billets + "€ TTC";
    }
    document.getElementById("prix").innerHTML = prix + "€ TTC";
}

function verifMail(){
    if(document.getElementById('email').value != document.getElementById('checkemail').value){
        document.getElementById('checkemail').setCustomValidity('Email de confirmation invalide') ;      
    }
    else{
        document.getElementById('checkemail').setCustomValidity('');      
    }
}
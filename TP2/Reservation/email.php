<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
    if(isset($_POST['boutonsubmit']) && isset($_COOKIE['billets']) && isset($_COOKIE['type']) && isset($_COOKIE['sexe']) && isset($_COOKIE['nom']) && isset($_COOKIE['prenom']) && isset($_COOKIE['adresse']) && isset($_COOKIE['codepostal']) && isset($_COOKIE['email']) && isset($_COOKIE['ville'])){

        if($_COOKIE['sexe'] == "H"){
            $sexe = 1;
        }
        else{
            $sexe = 0;
        }
        if($_COOKIE['type'] == "professionel"){
            $pro = 1;
        }
        else{
            $pro = 0;
        }
        
        $idUser = uniqid();
        $idCommande = uniqid();
        $idFacture = uniqid();
        if($db = mysqli_connect("localhost", "Hugo087", "abc", "Reservation")){
            $sql = $db->prepare("INSERT INTO Client (numclient, prenom, nom, sexe, adresse, codepostal, ville, email, professionel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $sql->bind_param("sssssssss" , $idUser, $_COOKIE['prenom'], $_COOKIE['nom'], $sexe, $_COOKIE['adresse'], $_COOKIE['codepostal'], $_COOKIE['ville'], $_COOKIE['email'], $pro);
            $sql->execute();
            $sql = $db->prepare("INSERT INTO Commande (numcommande, datecommande, numclient, numfacture) VALUES (?, now(), ?, ?)");
            $sql->bind_param("sss" , $idCommande, $idUser, $idFacture);
            $sql->execute();
            for($i = 0; $i < $_COOKIE['billets']; $i++){
                $sql = $db->prepare("INSERT INTO Billet (numbillet, numcommande, prix) VALUES (?, ?, ?)");
                $idBillet[] = uniqid();
                if($i == 0){
                    $valbi = 8;
                }
                else{
                    $valbi = 4;
                }
                $sql->bind_param("ssi" , $idBillet[$i], $idCommande, $valbi);
                $sql->execute();
            }
        }
        else{
            echo "<div style='text-align: center'>Impossible de contacter le serveur.</div>";
        }                                       
        $sql->close();
        mysqli_close($db);
    }
    else{
        header("Location: index.php");
        exit;
    }
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset="utf-8">
        <title> Salon de la décoration </title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <header>
            <div class="titre">Salon de la décoration</div>
            <div class="soustitre">Réception d'email</div>
        </header>
        <main>
            <label class="gris">De</label>
            <label>contact@salondeladecoration.fr</label>
            <br>
            <br>
            <label class="gris">A</label>
            <label><?=$_COOKIE['email']?></label>
            <br>
            <br>
            <br>
            <label class="gris">Contenu de l'email</label>
            <p class="email">
                Bonjour, <br><br>
                Vous venez de réserver vos billets pour le salon de la décoration qui se déroule les 6, 7, 8  Mars 2009 à Lille Grand Palais.<br>
                Vous trouverez en pièces jointes de cet email votre facture et vos billets au format PDF.<br><br>
                <b>Merci d'imprimer vos billets afin de les présenter à l'entrée du salon.</b><br><br>
                Chaque billet donne droit à une seule entrée, pour plus d'informations veuillez consulter les conditions générales de vente.<br><br>
                Cordialement,<br>
                L'équipe du Salon de la décoration.
            </p>
            <label class="gris">Pièces jointes de l'email</label>
            <p class="email">
            <?php
                echo "<a class='pdf' href='../PDF/facturepdf.php?facture=" . $idFacture .  "'><img src='../Ressources/pdf.png' alt='pdf' width='40'> <label style='width:17em'>facture_". $idFacture .".pdf</label></a>";
                for($i = 0; $i < sizeof($idBillet); $i++){
                    echo "<a class='pdf' href='../PDF/billetpdf.php?billet=" . $idBillet[$i] .  "'><img src='../Ressources/pdf.png' alt='pdf' width='40'> <label style='width:17em'>e-billet_". $idBillet[$i] .".pdf</label></a>";
                }
            ?>
            </p>
        </main>
    </body>
</html>
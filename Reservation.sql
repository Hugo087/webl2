-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 04 Mai 2019 à 18:01
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Reservation`
--

-- --------------------------------------------------------

--
-- Structure de la table `Billet`
--

CREATE TABLE `Billet` (
  `numbillet` char(13) NOT NULL,
  `datescan` datetime DEFAULT NULL,
  `numcommande` char(13) NOT NULL,
  `prix` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Billet`
--

INSERT INTO `Billet` (`numbillet`, `datescan`, `numcommande`, `prix`) VALUES
('5ccdb638883da', '2019-05-04 17:58:50', '5ccdb638878b8', 8),
('5ccdb63888761', '2019-05-04 17:59:55', '5ccdb638878b8', 4),
('5ccdb63888aef', NULL, '5ccdb638878b8', 4);

-- --------------------------------------------------------

--
-- Structure de la table `Client`
--

CREATE TABLE `Client` (
  `numclient` char(13) NOT NULL,
  `prenom` varchar(500) NOT NULL,
  `nom` varchar(500) NOT NULL,
  `sexe` tinyint(1) NOT NULL,
  `adresse` varchar(500) NOT NULL,
  `codepostal` varchar(500) NOT NULL,
  `ville` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `professionel` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Client`
--

INSERT INTO `Client` (`numclient`, `prenom`, `nom`, `sexe`, `adresse`, `codepostal`, `ville`, `email`, `professionel`) VALUES
('5ccdb638878b5', 'Hugo', 'Haquette', 1, '5 rue des peupliers ', '67210', 'Obernai', 'gaga@gmail.fr', 0);

-- --------------------------------------------------------

--
-- Structure de la table `Commande`
--

CREATE TABLE `Commande` (
  `numcommande` char(13) NOT NULL,
  `datecommande` date NOT NULL,
  `numclient` char(13) NOT NULL,
  `numfacture` char(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Commande`
--

INSERT INTO `Commande` (`numcommande`, `datecommande`, `numclient`, `numfacture`) VALUES
('5ccdb638878b8', '2019-05-04', '5ccdb638878b5', '5ccdb638878b9');

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `id` int(11) NOT NULL,
  `user` varchar(3000) NOT NULL,
  `mdp` varchar(3000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`id`, `user`, `mdp`) VALUES
(8, 'hugo', '$2y$10$QNHz2NegIKyLOEEg/s1Vg.eFX4R9I/ecLVOta/Mt1Fx9/rALd9.iu');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Billet`
--
ALTER TABLE `Billet`
  ADD PRIMARY KEY (`numbillet`),
  ADD KEY `FK_numcommande_Commande` (`numcommande`);

--
-- Index pour la table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`numclient`);

--
-- Index pour la table `Commande`
--
ALTER TABLE `Commande`
  ADD PRIMARY KEY (`numcommande`),
  ADD KEY `Fk_numclient_Client` (`numclient`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Billet`
--
ALTER TABLE `Billet`
  ADD CONSTRAINT `FK_numcommande_Commande` FOREIGN KEY (`numcommande`) REFERENCES `Commande` (`numcommande`);

--
-- Contraintes pour la table `Commande`
--
ALTER TABLE `Commande`
  ADD CONSTRAINT `Fk_numclient_Client` FOREIGN KEY (`numclient`) REFERENCES `Client` (`numclient`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 23 Avril 2019 à 17:45
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Parking`
--

-- --------------------------------------------------------

--
-- Structure de la table `CarteGrise`
--

CREATE TABLE `CarteGrise` (
  `Id_Pro` int(11) NOT NULL,
  `Immat` int(11) NOT NULL,
  `DateCarte` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `CarteGrise`
--

INSERT INTO `CarteGrise` (`Id_Pro`, `Immat`, `DateCarte`) VALUES
(1, 1275332, '2019-04-01'),
(2, 6554753, '2019-04-05'),
(3, 7765385, '2011-04-16'),
(4, 8765532, '2016-08-23');

-- --------------------------------------------------------

--
-- Structure de la table `Modele`
--

CREATE TABLE `Modele` (
  `Id_Mod` int(11) NOT NULL,
  `modele` varchar(500) NOT NULL,
  `carburant` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Modele`
--

INSERT INTO `Modele` (`Id_Mod`, `modele`, `carburant`) VALUES
(1, 'BMW X5', 'Essence '),
(2, 'Bugatti Veyron', 'Essence'),
(3, 'Clio', 'Gazoil'),
(4, 'Tesla S', 'Electrique'),
(5, 'rfefr', 'gfreg'),
(6, 'rfe', 'gfreg'),
(7, 'fza', 'fgza');

-- --------------------------------------------------------

--
-- Structure de la table `Proprietaire`
--

CREATE TABLE `Proprietaire` (
  `Id_Pro` int(11) NOT NULL,
  `nom` varchar(500) NOT NULL,
  `prenom` varchar(500) NOT NULL,
  `Adresse` varchar(500) NOT NULL,
  `code_postal` int(11) NOT NULL,
  `ville` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Proprietaire`
--

INSERT INTO `Proprietaire` (`Id_Pro`, `nom`, `prenom`, `Adresse`, `code_postal`, `ville`) VALUES
(1, 'Feisthauer', 'Arnaud', '3 rue des peupliers', 67115, 'plobsheim'),
(2, 'Haquette', 'Hugo', '9b rue de Bernardswiller', 67210, 'Obernai'),
(3, 'Tibi', 'Anthea', '4 rue des forêts', 59300, 'Valenciennes'),
(4, 'Delouvy', 'Lilian', '6 rue des champs', 59300, 'Valenciennes'),
(5, 'Mathis', 'Romaric', '1 rue du four Banal', 67200, 'Strasbourg');

-- --------------------------------------------------------

--
-- Structure de la table `Voiture`
--

CREATE TABLE `Voiture` (
  `Immat` int(11) NOT NULL,
  `Id_Mod` int(11) NOT NULL,
  `couleur` varchar(500) NOT NULL,
  `date_circulation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Voiture`
--

INSERT INTO `Voiture` (`Immat`, `Id_Mod`, `couleur`, `date_circulation`) VALUES
(1275332, 1, 'Bleu', '2005-04-10'),
(6554753, 3, 'Verte', '2019-04-01'),
(7765385, 2, 'Noir', '2017-04-09'),
(8765532, 4, 'Rouge', '2010-04-02');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `CarteGrise`
--
ALTER TABLE `CarteGrise`
  ADD PRIMARY KEY (`Id_Pro`,`Immat`),
  ADD KEY `FK_Immat_Voiture` (`Immat`);

--
-- Index pour la table `Modele`
--
ALTER TABLE `Modele`
  ADD PRIMARY KEY (`Id_Mod`);

--
-- Index pour la table `Proprietaire`
--
ALTER TABLE `Proprietaire`
  ADD PRIMARY KEY (`Id_Pro`);

--
-- Index pour la table `Voiture`
--
ALTER TABLE `Voiture`
  ADD PRIMARY KEY (`Immat`),
  ADD KEY `FK_IdMod_Modele` (`Id_Mod`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Modele`
--
ALTER TABLE `Modele`
  MODIFY `Id_Mod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `Proprietaire`
--
ALTER TABLE `Proprietaire`
  MODIFY `Id_Pro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `CarteGrise`
--
ALTER TABLE `CarteGrise`
  ADD CONSTRAINT `FK_IdPro_Proprietaire` FOREIGN KEY (`Id_Pro`) REFERENCES `Proprietaire` (`Id_Pro`),
  ADD CONSTRAINT `FK_Immat_Voiture` FOREIGN KEY (`Immat`) REFERENCES `Voiture` (`Immat`);

--
-- Contraintes pour la table `Voiture`
--
ALTER TABLE `Voiture`
  ADD CONSTRAINT `FK_IdMod_Modele` FOREIGN KEY (`Id_Mod`) REFERENCES `Modele` (`Id_Mod`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
